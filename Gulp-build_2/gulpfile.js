"use strict"
const {series} = require('gulp');
const browsersync = require("browser-sync").create();
const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const del = require('del');
const sourcemaps = require('gulp-sourcemaps');
const htmlmin = require('gulp-htmlmin');
const size = require('gulp-size');
const jsmin = require('gulp-uglify');
const concat = require('gulp-concat');
const rename = require("gulp-rename");
const cleanCSS = require("gulp-clean-css");
const cssmin = require('gulp-csso');
const autoprefixer = require('gulp-autoprefixer');
const imagesmin = require('gulp-imagemin');
const newer = require('gulp-newer');
const plumber = require('gulp-plumber');
const notify = require("gulp-notify");


const path = {
    src: {
        root: "./src",
        scss: "./src/scss",
        js: "./src/js",
        img: "./src/img/**",
        html: "./*.html",
    },

    dist: {
        root: "./dist",
        css: "./dist",
        js: "./dist",
        img: "./dist/img",
        html: "./dist",

    },
};

function html() {
    return gulp.src(`${path.src.html}/**/*.html`)
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(size({
            showFiles: true
        }))
        .pipe(concat('index.html'))
        .pipe(gulp.dest(path.dist.html))
        .pipe(browsersync.stream())
}


function scripts() {
    return gulp
        .src(`${path.src.js}/*.js`)
        .pipe(
            plumber({
                errorHandler: notify.onError((error) => ({
                    title: "Scripts",
                    message: error.message,
                })),
            })
        )
        .pipe(sourcemaps.init())
        .pipe(concat('scripts.js'))
        .pipe(rename({
            basename: "scripts",
            suffix: ".min",
        }))
        .pipe(jsmin())
        .pipe(sourcemaps.write())
        .pipe(size({
            showFiles: true
        }))
        .pipe(gulp.dest(path.dist.js))
    // .pipe(browsersync.stream())
}


function styles() {
    return gulp
        .src(`${path.src.scss}/**/*.scss`)
        .pipe(
            plumber({
                errorHandler: notify.onError((error) => ({
                    title: "Styles",
                    message: error.message,
                })),
            })
        )
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({
            level: 2
        }))
        // .pipe(concat('styles.css'))//что ты обьединяешь? если импортишь то concat делать не нужно если нет импортов то нужен concat
        .pipe(rename({
            basename: "styles",
            suffix: ".min",
        }))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(size({
            showFiles: true
        }))
        .pipe(gulp.dest(path.dist.css))
    // .pipe(browsersync.stream())
}


function images() {
    return gulp
        .src(`${path.src.img}/**/*.{png,jpg,jpeg,gif,svg}`)
        .pipe(newer(path.dist.img))
        .pipe(imagesmin([
            imagesmin.gifsicle({interlaced: true}),
            imagesmin.mozjpeg({quality: 75, progressive: true}),
            imagesmin.optipng({optimizationLevel: 5})]))
        .pipe(size({
            showFiles: true
        }))
        .pipe(gulp.dest(path.dist.img))
}


function clean() {
    return del(['dist/*']);
}


function watch() {
    browsersync.init({
        server: {
            file: "./index.html"
        }
    })

    // gulp.watch(path.src.html).on('change', browsersync.reload)
    gulp.watch(path.src.scss, styles).on('change', browsersync.reload)
    gulp.watch(path.src.html, html).on('change', browsersync.reload)
    gulp.watch(path.src.js, scripts).on('change', browsersync.reload)
    gulp.watch(path.src.img, images).on('change', browsersync.reload)
}


const build = series(clean, styles, scripts, images);
const dev = series(build, watch)

exports.build = build;
exports.dev = dev;
exports.default = build;


//1.удалила babel, в этом проете он нам не нужен и просто делает еще больше проект) мы же не пишем по старым стандартам)
//2..pipe(browsersync.stream()) - это там не нужно, лишнее и у нас задача перезагружать страницу при изменении сss и js а stream страницу не перезагружает
//3.удалила функцию clean заменила название fclean на clean но в принципе для разработки можно было бы оставить
//4.изменила server в функции watch потому что если ты ставишь ему server: baseDir он следит за всей папкой!это лишняя нагрузка на комп
//server: file позволяет запускать только файл index.html
//5.добавила в package.json script задачи которие запускаются(npm run dev, npm run build)
//6.добавила перезагрузку сервера при изменении в script, image, styles
//7. эта строка(gulp.watch(path.src.html).on('change', browsersync.reload)) и эта(gulp.watch(path.src.html, html) почти дубликаты, обьединила в один
//8.Убрала concat с styles() /.pipe(concat('styles.css'))//что ты обьединяешь? если импортишь то concat делать не нужно если нет импортов то нужен concat[:)]
//9. const build = series(clean, styles, scripts, images);
// const dev = series(build, watch)
//задачи выше должны быть независимы друг от друга в dev нам нужно сначала собрать проект а потом следить за всеми папками и файлами




